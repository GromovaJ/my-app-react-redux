import React, { Component } from 'react';

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false};
  }
  toggleState() {
    this.setState({ isOpened: !this.state.isOpened })
  }
  render() {
    console.log('isOpened', this.state.isOpened );
    let dropdownText;
    if (this.state.isOpened) {
      dropdownText = <div>isOpened: true</div>;
    }
    return (
      <div onClick={this.toggleState.bind(this)}>
        Its dropdown baby
        {dropdownText}
      </div>
    );
  }
}

export default Dropdown;
