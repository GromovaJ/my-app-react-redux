const initialState = [];
export default function tracks(state = initialState, action) {
  if (action.type === 'ADD_TRACK') {
    return [
      ...state,
      action.playload
    ];
  } else if (action.type === 'FETCH_TRACKS_SUCCESS') {
    return action.playload
  }
  return state;
}
